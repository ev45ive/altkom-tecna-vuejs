import { Album, AlbumView } from "./Album"

const TypeScriptExamples = {}
type AlbumId = Album['id']
type AlbumKeys = keyof Album 
type PartialAlbum = {
    [key in AlbumKeys]?: Album[key] 
}
type Partial1<T extends Album> = {
    [key in AlbumKeys]?: T[key] 
}
type Partial2<T> = {
    [key in keyof T]?: T[key] 
}
type PartialAlbum2 = Partial<Album>

type PartialAlbum3 = Pick<Album, 'id'|'name'> & {
    images: Pick<Album['images'][0],'url'>[]
}

type someKeys = Extract<keyof Album,'id'|'name'|'placki'>

const album: AlbumView = { id: '', name: '', type: 'album', images: [] }
// const albumCache: Readonly<AlbumView> = album
const albumCache: Readonly<typeof album> = album
// albumCache.id = '123' // Cannot assign to 'id' because it is a read-only property.


interface Point{x:number, y: number}
interface Vector{x:number, y: number, length: number}

let p:Point; 
let v:Vector; 

p = v 
// v = p // Property 'length' is missing in type 'Point' but required in type 'Vector'.
// p.length // Property 'length' does not exist on type 'Point'
