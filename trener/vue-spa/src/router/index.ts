import { createRouter,/*  createWebHashHistory, */ createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'
import PlaylistsVuex from '../views/PlaylistsVuex.vue'
import AlbumsSearch from '../views/search/AlbumsSearch.vue'
import AlbumDetails from '../views/albums/AlbumDetails.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/playlists/:id?/:mode?/',
    name: 'Playlists',
    component: PlaylistsVuex,
  }, {
    path: '/albums/:id',
    name: 'AlbumDetails',
    component: AlbumDetails,
  },
  {
    path: '/search',
    name: 'Search',
    // component: AlbumsSearch,
    component: () => import(/* webpackChunkName: "search" */ '../views/search/AlbumsSearch.vue')
    // async beforeEnter(to, from, next) {
    //   /* if user == admin  */ next()
    // }
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = createRouter({
  // history: createWebHashHistory(),
  history: createWebHistory(),
  linkActiveClass: 'active',
  routes,
})
// router.beforeEach(async (to, from) => {
//   // canUserAccess() returns `true` or `false`
//   const canAccess = await canUserAccess(to)
//   if (!canAccess) return '/login'
// })

export default router
