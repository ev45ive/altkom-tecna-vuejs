
export interface Playlist {
    id: string;
    name: string;
    public: boolean;
    description: string;
}
/* 
// ZOD:

if(process.env.NODE_ENV === 'development'){
    const playlistSchema = z.shape({
        id: z.string(),
        name: z.string(),
        public: z.boolean(),
        description: z.string(),
    })
    type IPlaylist = z.Infer<typeof playlistSchema>

    function assertSchema<T>(data: any, schema: any): asserts data is T {
        schema.parse(data)
    }
}
 */