// @ts-nocheck

import { mockAlbums } from '@/services/MusicAPI';
// import { Meta, Story } from '@storybook/vue3'
import AlbumGrid from '../components/albums/AlbumsGrid.vue'

export default {
  title: 'Music Search/AlbumGrid',
  component: AlbumGrid,
} // as Meta<typeof AlbumGrid>

const Template/* : Story<typeof AlbumGrid> */ = (args, { argTypes }) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { AlbumGrid },
  // props: Object.keys(argTypes),
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    // Story args can be spread into the returned object
    return { args };
  },
  // Then, the spread values can be accessed directly in the template
  template: '<album-grid :results="args.results" />',
});

export const EmptyGrid = Template.bind({});
EmptyGrid.args = {
  results: [],
};

export const NotEmptyGrid = Template.bind({});
NotEmptyGrid.args = {
  results: mockAlbums,
};