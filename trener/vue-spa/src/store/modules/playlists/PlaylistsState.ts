import { Playlist } from "@/components/playlists/Playlist";


export interface PlaylistsState {
    // selectedId: string | null;
    entities: {
        playlists: {
            [key: string]: Playlist;
        };
    };
    lists: {
        myplaylists: Playlist['id'][];
        topplaylists: Playlist['id'][];
        sharedplaylists: Playlist['id'][];
    };
}
