import { Playlist } from '@/components/playlists/Playlist';
import { createPlaylistAPI, getCurrentUserPlaylists, updatePlaylistAPI } from '@/services/MusicAPI';
import { playlistData } from '@/store/mocks/playlistData';
import { Module } from 'vuex';
import { RootState } from "./../../RootState";
import { PlaylistsState } from './PlaylistsState';

export const playlists: Module<PlaylistsState, RootState> = {
    namespaced: true,
    state: {
        // playlistsData: playlistData,
        // selectedId: "234",
        entities: { playlists: {} },
        lists: { myplaylists: [], sharedplaylists: [], topplaylists: [] }
    },
    mutations: {
        PLAYLISTS_LOADED(state, payload: Playlist[]) {

            state.lists.myplaylists = payload.map(p => {
                state.entities.playlists[p.id] = p
                return p.id
            })
        },
        PLAYLIST_UPDATED(state, payload: Playlist) {
            state.entities.playlists[payload.id] = payload
        }
    },
    actions: {
        async loadPlaylists({ state, commit, dispatch, getters }) {
            const payload = await getCurrentUserPlaylists()
            commit('PLAYLISTS_LOADED', payload)
        },

        async updatePlaylist({ state, commit, dispatch, getters }, draft) {
           debugger
            const saved = await updatePlaylistAPI(draft)
            commit('PLAYLIST_UPDATED', saved)
            // const payload = await updatePlaylistItems(draft, items)
            // commit('PLAYLIST_ITEMS_UPDATED', draft)
            return saved
        },

        async createPlaylist({ state, commit, dispatch, getters }, draft) {
            debugger
            const saved = await createPlaylistAPI(draft)
            commit('PLAYLIST_UPDATED', saved)
            await dispatch('loadPlaylists')
            return saved
        },

    },
    getters: {
        myplaylists(state) {
            return state.lists.myplaylists.map(id => state.entities.playlists[id])
        },
        sharedplaylists(state) {
            return state.lists.sharedplaylists.map(id => state.entities.playlists[id])
        }
    },
    modules: {

    },
}
